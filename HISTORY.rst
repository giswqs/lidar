=======
History
=======
0.1.6 (2018-05-21)
------------------
0.1.5 (2018-05-16)
------------------
0.1.3 (2018-05-15)
------------------
0.1.0 (2018-05-14)
------------------
* First release on PyPI.
